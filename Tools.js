//Petit module permettant de comparer deux cours suivant la catégorie de l'objet course - celà me permet de les trier dans le Module CustomParser
module.exports = function (a, b) {
    // Use toUpperCase() to ignore character casing
    const bandA = a.Categorie.toUpperCase();
    const bandB = b.Categorie.toUpperCase();
  
    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison;
  };
 