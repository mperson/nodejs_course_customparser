/*Librairies externes*/
const axios = require('axios'); //https://github.com/axios/axios#readme
const cheerio = require('cheerio'); //https://github.com/cheeriojs/cheerio#readme
const fs = require('fs');
/*Modules internes*/
const Compare = require("./Tools");
const Course = require('../Models/Course.js');

var CustomParser = {
     getPage:function (url)  {
        try {
          //Utilisation de la libraire axios pour récupérer la page html 
          return axios.get(url)
        } catch (error) {
          console.error(error)
        }
      },
      savePage :function( data, path ) {
        //Content contiendra le code source de la page html, les \r\n ne sont que pour la mise en page textuelle du fichier et non HTML
        let contents = "<!DOCTYPE>" + '\n\n';
            contents += '<html>\n\n<head>\n\n<title>\n\nFormations\n\n</title>\n\n<body>\n\n';
            contents +=  "<h1>Formations</h1>\n\n";
        data= data.sort(Compare); //Utilisation de mon module Tools pour trier mes données sur la catégorie
        let current ='';
            data.forEach(element => {
                  if(current!=element.Categorie)
                  {
                    if(current!=='') contents+="</ul>\r\n<hr>\r\n";
                    current = element.Categorie;
                    contents+="<h2>"+ current +"</h2>\r\n<ul>";
                    
                  }
                 
                  contents+= "<li>"+ element.Infos +"</li>\r\n";
    
                  
            });
    
            contents += '</body>\n\n</html>';
            //écriture de mon Fichier de manière synchrone
            fs.writeFileSync(path, contents);
    },
    parsePage:function(data) {
        const $ = cheerio.load(data); //Utilisation de la librairie cheerio pour obtenir un outils de parsing basé sur l'html et utilisant les balises "jquery" pour la récupération
        let output = []; 
        //Recherche des éléments html ayant la classe course-block
        $(".course-block").each( (i, elem ) => {
            let titre = $(elem).find( "h3[class='course-title']" ).text(); //Recherche dans l'élément le titre de la formation basé sur la balise h3 ayant la classe course-title
            let categorie = $(elem).find("div[class='course-thumbnail']").find("img").attr('alt'); //Récupération de l'attribut alt de l'image se trouvant dans la div ayant la classe course-thumbnail
            let lien ="http://www.cognitic.be/"+ $(elem).find("div[class='course-thumbnail']").find("a").attr('href'); //Récupération de l'attribut href pour récupérer l'url de la balise a de la div ayant la classe course-thumbnail
            let description = $(elem).find("div[class='course-content']").find("div[class='description']").find("p").text(); //Récupéreation du contenu de la balise p se trouvant dans la div ayant la classe description sous ala div ayant la classe course-content
            let Duree = $(elem).find("div[class='course-content']").find("div[class='course-duration ']").text();//récupération du contenu de la balise Div ayant la classe course-duration se trouvant dans la div ayant la classe course-content
            let current = new Course(titre,categorie,description,lien,Duree ); //instanciation d'un objet course avvec les données récupérées
         
             //On dépose l'objet course créé dans le tableau
             output.push(current);
        });
        return output;
    }
    
};

module.exports = CustomParser